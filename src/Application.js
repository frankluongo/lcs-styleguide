import React from "react";

import Layout from "./UI/Layout";

import "../assets/stylesheets/pages/homepage.scss";

const Application = () => {
  return (
    <Layout mainBg="bg-lcs-blue-4">
      <section className="homepage__container container layout flex-container align-items-center">
        <div className="homepage__content">
          <h1 className="xlarge m-xsmall-bottom-1 m-medium-bottom-4 text-gray-1">
            Colorado <br />
            Legislative Services <br />
            Styleguide
          </h1>
          <p className="xlarge text-gray-1">
            This style guide contains the color, typography, elements,
            components and more that define the visual appearance of all
            websites and web applications produced by the Legislative Council
            Services for the Colorado General Assembly and any other clients for
            whom we provide services.
          </p>
        </div>
      </section>
    </Layout>
  );
};

export default Application;
