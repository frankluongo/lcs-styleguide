/*
  Message Box Types
  info
  error
  success
  warning
*/

import React from "react";

const MessageBox = ({ type, title, content }) => {
  return (
    <div className={`message-box message-box--${type}`}>
      <div className="message-box__heading">
        <span className="fa fa-info-circle message-box__icon"></span>
        <strong className="message-box__title h3">{title}</strong>
      </div>
      <hr className="message-box__rule" />
      <div className="message-box__copy">{content}</div>
    </div>
  );
};

export default MessageBox;
