import React, { Fragment } from "react";

const Tabs = ({ tabs, tabIdentifier }) => {
  const Tab = ({ content, index }) => {
    const tabGroup = `${tabIdentifier}-tabs`;
    const tabId = `${tabIdentifier}-tab-${index}`;
    const isChecked = index === 0;

    return (
      <Fragment>
        <input
          name={tabGroup}
          type="radio"
          id={tabId}
          defaultChecked={isChecked}
          className="tab__input visually-hidden"
        />
        <label htmlFor={tabId} className="tab__label">
          {content.tabLabel}
        </label>
        <div className="tab__panel">{content.tabContent}</div>
      </Fragment>
    );
  };

  return (
    <section className="tabs">
      {tabs.map((tab, index) => {
        return <Tab content={tab} index={index} key={index} />;
      })}
    </section>
  );
};

Tabs.defaultProps = {
  tabIdentifier: "example",
  tabs: [
    {
      tabLabel: "Tab 1",
      tabContent: (
        <Fragment>
          <h1 className="m-xsmall-bottom-1 m-small-bottom-2 m-xsmall-top-1 m-small-top-2">
            This is a heading
          </h1>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus
            perspiciatis, optio sit ullam aliquid facilis sed enim dolor omnis
            ipsa vitae quod temporibus quidem accusantium ex explicabo suscipit
            ipsum voluptatibus!
          </p>
        </Fragment>
      )
    },
    {
      tabLabel: "Tab 2",
      tabContent: (
        <Fragment>
          <h1 className="m-xsmall-bottom-1 m-small-bottom-2 m-xsmall-top-1 m-small-top-2">
            This is another heading
          </h1>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus
            perspiciatis, optio sit ullam aliquid facilis sed enim dolor omnis
            ipsa vitae quod temporibus quidem accusantium ex explicabo suscipit
            ipsum voluptatibus!
          </p>
        </Fragment>
      )
    }
  ]
};

export default Tabs;
