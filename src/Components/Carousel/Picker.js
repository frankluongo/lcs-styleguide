import React from "react";

const Picker = ({ activeIndex, handler, title, index }) => {
  const isActive = activeIndex === index ? "active" : "";

  return (
    <button
      aria-label={title}
      className={`carousel__picker ${isActive}`}
      onClick={() => {
        handler(index);
      }}
    />
  );
};

Picker.defaultProps = {
  activeIndex: 0,
  handler: () => {
    console.log("hi");
  },
  index: 0,
  title: "Sample Title"
};

export default Picker;
