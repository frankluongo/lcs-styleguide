import React from "react";

const Slide = ({ activeIndex, index, slide }) => {
  const isActive = index === activeIndex ? "active" : "";
  return (
    <li data-index={index} className={`carousel__slide ${isActive}`}>
      <figure
        className="carousel-slide__image"
        style={{ backgroundImage: `url(${slide.image})` }}
      />
      <div className="carousel-slide__content">
        <div className="h1">{slide.title}</div>
        <hr className="carousel-slide__divider" />
        {slide.content}
      </div>
    </li>
  );
};

Slide.defaultProps = {
  activeIndex: 0,
  index: 0,
  slide: {
    content: (
      <p>
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reiciendis
        dicta aliquam iure, ipsa in, veritatis eveniet, debitis sed ab omnis
        praesentium adipisci culpa? Odio cupiditate voluptate officiis optio
        tenetur ab?
      </p>
    ),
    image: "https://placekitten.com/1200/800",
    title: "Slide Title 1"
  }
};

export default Slide;
