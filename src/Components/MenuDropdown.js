import React, { useState, useRef } from "react";
import { animated, useSpring } from "react-spring";
import useMeasure from "use-measure";

const MenuDropdown = ({ disabled }) => {
  const [on, toggle] = useState(false);
  const elemRef = useRef();
  const { height, top } = useMeasure(elemRef);
  const animation = useSpring({
    opacity: on ? 1 : 0,
    height: on ? menuDimensions() : 0,
    pointerEvents: on ? "all" : "none"
  });

  function menuDimensions() {
    return height + top * 2;
  }

  function toggleDropdown() {
    toggle(!on);
  }

  return (
    <nav
      className={`dropdown-menu ${on && "active"}`}
      aria-label="Dropdown"
      data-js="Dropdown"
      disabled={disabled}
      aria-disabled={disabled}
    >
      <button
        className="dropdown-menu__button"
        onClick={toggleDropdown.bind(this)}
      >
        Dropdown Menu
      </button>
      <span
        className={`dropdown-menu__icon fa ${
          on ? "fa-caret-left" : "fa-caret-down"
        }`}
      />
      <animated.div className="dropdown-menu-list__wrapper" style={animation}>
        <ul
          ref={elemRef}
          className="dropdown-menu__list menu"
          aria-expanded={on}
        >
          <li className="dropdown-menu-list__item">
            <a className="dropdown-menu-list__link" href="#">
              List Item 1
            </a>
          </li>
          <li className="dropdown-menu-list__item">
            <a className="dropdown-menu-list__link" href="#">
              List Item 1
            </a>
          </li>
          <li className="dropdown-menu-list__item">
            <a className="dropdown-menu-list__link" href="#">
              List Item 1
            </a>
          </li>
        </ul>
      </animated.div>
    </nav>
  );
};

MenuDropdown.defaultProps = {
  disabled: false
};

export default MenuDropdown;
