import React, { useState, Fragment } from "react";
import { animated, useSpring } from "react-spring";

const Modal = ({ children, title }) => {
  const [on, toggle] = useState(false);

  const modalAnimation = useSpring({
    pointerEvents: on ? "all" : "none",
    opacity: on ? 1 : 0
  });

  const modalContentAnimation = useSpring({
    transform: on ? `translate3d(0,0,0)` : `translate3d(0,50%,0)`
  });

  function toggleState() {
    toggle(!on);
  }

  return (
    <Fragment>
      <button onClick={toggleState.bind(this)}>Toggle Modal</button>
      <animated.div aria-hidden={!on} className="modal" style={modalAnimation}>
        <animated.div className="modal__content" style={modalContentAnimation}>
          <div className="modal-content__header">
            <div className="modal-content-header__title h2">{title}</div>
            <button
              aria-label="Close Modal"
              className="modal__close"
              onClick={toggleState.bind(this)}
            >
              <span className="fa fa-times modal__close-icon" />
            </button>
          </div>
          {children}
        </animated.div>
      </animated.div>
    </Fragment>
  );
};

Modal.defaultProps = {
  title: "Modal Header"
};

export default Modal;
