import React, { useState, useRef, Fragment } from "react";
import { animated, useSpring } from "react-spring";
import useMeasure from "use-measure";

const AccordionPanel = ({ title, content }) => {
  const [open, setAccordionPanelState] = useState(false);
  const elemRef = useRef();
  const { height, top } = useMeasure(elemRef);

  const animation = useSpring({
    height: open ? accordionPanelDimensions() : 0
  });

  function accordionPanelDimensions() {
    return height + top * 2;
  }

  function toggleContent() {
    setAccordionPanelState(!open);
  }

  return (
    <Fragment>
      <button
        onClick={toggleContent}
        className="accordion__title"
        aria-hidden={!open}
      >
        {title}
        <span className="accordion-title__icon fa fa-chevron-circle-down" />
      </button>
      <animated.div className="accordion__panel-wrapper" style={animation}>
        <section ref={elemRef} className="accordion__panel">
          {content}
        </section>
      </animated.div>
    </Fragment>
  );
};

export default AccordionPanel;
