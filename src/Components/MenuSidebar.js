import React from "react";

const MenuSidebar = () => {
  return (
    <aside className="sidebar-menu">
      <section className="sidebar-menu__header">
        <h3>Sidebar Menu Title</h3>
      </section>
      <ul className="sidebar-menu__list menu">
        <li>list item 1</li>
        <li>list item 1</li>
        <li>list item 1</li>
        <li>list item 1</li>
        <li>list item 1</li>
      </ul>
    </aside>
  );
};

export default MenuSidebar;
