import React from "react";

const NotificationBar = () => {
  return (
    <div className="notification-bar">
      <div className="container">
        <strong>Notification Bar Content</strong>
      </div>
    </div>
  );
};

export default NotificationBar;
