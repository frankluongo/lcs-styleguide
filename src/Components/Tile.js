import React from "react";

const Tile = ({ image, text, url }) => {
  return (
    <a
      className="tile has-background-image"
      href={url}
      style={{ backgroundImage: `url(${image})` }}
    >
      <div className="tile__content">{text}</div>
    </a>
  );
};

Tile.defaultProps = {
  image: "http://placekitten.com/640/640",
  text: "Tile link",
  url: "https://google.com"
};

export default Tile;
