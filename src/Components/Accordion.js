import React from "react";

import AccordionPanel from "./AccordionPanel";

const Accordion = ({ panels }) => {
  return (
    <div className="accordion">
      {panels.map((panel, index) => (
        <AccordionPanel
          title={panel.title}
          content={panel.content}
          key={index}
        />
      ))}
    </div>
  );
};

export default Accordion;
