import React from "react";

const Card = ({ image, title, description, url, cta }) => {
  return (
    <a className="card" href={url}>
      <div
        className="card__image"
        style={{ backgroundImage: `url(${image})` }}
      />
      <div className="card__content">
        <div className="card__title h3 m-xsmall-bottom-1">{title}</div>
        <div className="card__description m-xsmall-bottom-1">
          <p>{description}</p>
        </div>
        <div className="card__cta">{cta}</div>
      </div>
    </a>
  );
};

export default Card;
