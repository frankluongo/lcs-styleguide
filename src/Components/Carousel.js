import React, { useState } from "react";

import Picker from "./Carousel/Picker";
import Slide from "./Carousel/Slide";

const Carousel = ({ slides }) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const limit = slides.length - 1;

  return (
    <section className="carousel">
      <ul className="carousel__content">
        {slides.map((slide, index) => (
          <Slide
            activeIndex={activeIndex}
            index={index}
            key={index}
            slide={slide}
          />
        ))}
      </ul>
      <div className="carousel__pickers">
        {slides.map(({ title }, index) => (
          <Picker
            activeIndex={activeIndex}
            handler={goToSlide}
            index={index}
            key={index}
            title={title}
          />
        ))}
        <div className="carousel__controls">
          <button
            aria-label="Previous Slide"
            className="carousel__control previous"
            onClick={goToPrevSlide.bind(this)}
          >
            <span className="fa fa-chevron-left"></span>
          </button>
          <button
            aria-label="Next Slide"
            className="carousel__control next"
            onClick={goToNextSlide.bind(this)}
          >
            <span className="fa fa-chevron-right"></span>
          </button>
        </div>
      </div>
    </section>
  );

  function goToSlide(index) {
    setActiveIndex(index);
  }

  function goToNextSlide() {
    const next = activeIndex + 1;
    if (next > limit) {
      setActiveIndex(0);
    } else {
      setActiveIndex(next);
    }
  }

  function goToPrevSlide() {
    const prev = activeIndex - 1;
    if (prev < 0) {
      setActiveIndex(limit);
    } else {
      setActiveIndex(prev);
    }
  }
};

Carousel.defaultProps = {
  slides: [
    {
      content: (
        <p>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reiciendis
          dicta aliquam iure, ipsa in, veritatis eveniet, debitis sed ab omnis
          praesentium adipisci culpa? Odio cupiditate voluptate officiis optio
          tenetur ab?
        </p>
      ),
      image: "https://placekitten.com/1200/800",
      title: "Slide Title 1"
    },
    {
      title: "Slide Title 2",
      image: "https://placekitten.com/1200/800",
      content: (
        <p>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reiciendis
          dicta aliquam iure, ipsa in, veritatis eveniet, debitis sed ab omnis
          praesentium adipisci culpa? Odio cupiditate voluptate officiis optio
          tenetur ab?
        </p>
      )
    },
    {
      title: "Slide Title 3",
      image: "https://placekitten.com/1200/800",
      content: (
        <p>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reiciendis
          dicta aliquam iure, ipsa in, veritatis eveniet, debitis sed ab omnis
          praesentium adipisci culpa? Odio cupiditate voluptate officiis optio
          tenetur ab?
        </p>
      )
    }
  ]
};

export default Carousel;
