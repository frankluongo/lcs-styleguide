import React from "react";

const SearchForm = () => {
  function handleFormSubmit(event) {
    event.preventDefault();
    console.log("searching!");
  }

  return (
    <form
      className="search-form"
      onSubmit={handleFormSubmit.bind(this)}
      role="search"
    >
      <label className="visually-hidden" htmlFor="query">
        Search Application
      </label>
      <input className="search-form__input" id="query" type="search" />
      <button aria-label="Search" className="search-form__button" type="submit">
        <span className="fa fa-search" />
      </button>
    </form>
  );
};

export default SearchForm;
