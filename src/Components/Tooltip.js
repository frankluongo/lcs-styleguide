import React, { useState } from "react";
import { animated, useSpring } from "react-spring";

const Tooltip = ({ children, modifierClases }) => {
  const [on, toggle] = useState(false);
  const animation = useSpring({
    opacity: on ? 1 : 0,
    pointerEvents: on ? "all" : "none",
    transform: on ? "translate3d(1rem, -50%, 0)" : "translate3d(0rem, -50%, 0)"
  });

  function toggleTooltip() {
    toggle(!on);
  }

  return (
    <span className={`tooltip__container ${modifierClases}`}>
      <button
        aria-label="Show/Hide Tooltip Information"
        className="tooltip__button"
        onClick={toggleTooltip.bind(this)}
      >
        <span className="fa fa-info-circle tooltip-button__icon" />
      </button>
      <animated.span className="tooltip" style={animation}>
        {children}
      </animated.span>
    </span>
  );
};

Tooltip.defaultProps = {
  children: "Hello there!",
  modifierClases: "m-left-05"
};

export default Tooltip;
