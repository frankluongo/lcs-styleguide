import React, { useState, useRef } from "react";
import { animated, useSpring } from "react-spring";
import useMeasure from "use-measure";

const MenuMeatballs = () => {
  const [open, setMenuState] = useState(false);

  const elemRef = useRef();
  const { height, top } = useMeasure(elemRef);

  const animation = useSpring({
    height: open ? menuDimensions() : 0,
    pointerEvents: open ? "all" : "none"
  });

  function menuDimensions() {
    return height + top * 2;
  }

  const toggleMeatballMenuState = () => {
    setMenuState(!open);
  };

  return (
    <div className="meatball-menu__container">
      <button
        aria-label="View More Options"
        className={`meatball-menu__toggle ${open && "active"}`}
        onClick={toggleMeatballMenuState.bind(this)}
      >
        <span className="meatball-menu-toggle__dot col-1" />
        <span className="meatball-menu-toggle__dot col-2" />
        <span className="meatball-menu-toggle__dot col-3" />
      </button>
      <animated.div className="meatball-menu-list__wrapper" style={animation}>
        <ul ref={elemRef} className="meatball-menu menu">
          <li className="meatball-menu__item">Item 1</li>
          <li className="meatball-menu__item">Item 2</li>
          <li className="meatball-menu__item">Item 3</li>
        </ul>
      </animated.div>
    </div>
  );
};

export default MenuMeatballs;
