import React, { useState, Fragment } from "react";
import { useSpring, animated } from "react-spring";

const MenuBento = () => {
  const [open, setMenuState] = useState(false);

  const animation = useSpring({
    opacity: open ? 1 : 0,
    pointerEvents: open ? "all" : "none"
  });

  const toggleBentoMenu = () => {
    setMenuState(!open);
  };

  return (
    <Fragment>
      <button
        aria-label="View Options"
        className="bento-menu__toggle toggle"
        onClick={toggleBentoMenu.bind(this)}
      >
        <span className="bento-menu-toggle__dot toggle__item row-1 col-1 dot-1" />
        <span className="bento-menu-toggle__dot toggle__item row-1 col-2 dot-2" />
        <span className="bento-menu-toggle__dot toggle__item row-1 col-3 dot-3" />
        <span className="bento-menu-toggle__dot toggle__item row-2 col-1 dot-4" />
        <span className="bento-menu-toggle__dot toggle__item row-2 col-2 dot-5" />
        <span className="bento-menu-toggle__dot toggle__item row-2 col-3 dot-6" />
        <span className="bento-menu-toggle__dot toggle__item row-3 col-1 dot-7" />
        <span className="bento-menu-toggle__dot toggle__item row-3 col-2 dot-8" />
        <span className="bento-menu-toggle__dot toggle__item row-3 col-3 dot-9" />
      </button>
      <animated.div className="bento-menu__wrapper" style={animation}>
        <button
          aria-label="Close Menu"
          className="bento-menu__close"
          onClick={toggleBentoMenu.bind(this)}
        >
          <span className="fa fa-times" />
        </button>
        <ul className="bento-menu">
          <li className="bento-menu__item">
            <a className="bento-menu-item__link link" href="#">
              <div className="bento-menu-item-link__image" />
              Link to Thing
            </a>
          </li>
          <li className="bento-menu__item">
            <a className="bento-menu-item__link link" href="#">
              <div className="bento-menu-item-link__image" />
              Link to Thing
            </a>
          </li>
          <li className="bento-menu__item">
            <a className="bento-menu-item__link link" href="#">
              <div className="bento-menu-item-link__image" />
              Link to Thing
            </a>
          </li>
          <li className="bento-menu__item">
            <a className="bento-menu-item__link link" href="#">
              <div className="bento-menu-item-link__image" />
              Link to Thing
            </a>
          </li>
          <li className="bento-menu__item">
            <a className="bento-menu-item__link link" href="#">
              <div className="bento-menu-item-link__image" />
              Link to Thing
            </a>
          </li>
          <li className="bento-menu__item">
            <a className="bento-menu-item__link link" href="#">
              <div className="bento-menu-item-link__image" />
              Link to Thing
            </a>
          </li>
          <li className="bento-menu__item">
            <a className="bento-menu-item__link link" href="#">
              <div className="bento-menu-item-link__image" />
              Link to Thing
            </a>
          </li>
          <li className="bento-menu__item">
            <a className="bento-menu-item__link link" href="#">
              <div className="bento-menu-item-link__image" />
              Link to Thing
            </a>
          </li>
          <li className="bento-menu__item">
            <a className="bento-menu-item__link link" href="#">
              <div className="bento-menu-item-link__image" />
              Link to Thing
            </a>
          </li>
          <li className="bento-menu__item">
            <a className="bento-menu-item__link link" href="#">
              <div className="bento-menu-item-link__image" />
              Link to Thing
            </a>
          </li>
          <li className="bento-menu__item">
            <a className="bento-menu-item__link link" href="#">
              <div className="bento-menu-item-link__image" />
              Link to Thing
            </a>
          </li>
          <li className="bento-menu__item">
            <a className="bento-menu-item__link link" href="#">
              <div className="bento-menu-item-link__image" />
              Link to Thing
            </a>
          </li>
          <li className="bento-menu__item">
            <a className="bento-menu-item__link link" href="#">
              <div className="bento-menu-item-link__image" />
              Link to Thing
            </a>
          </li>
        </ul>
      </animated.div>
    </Fragment>
  );
};

export default MenuBento;
