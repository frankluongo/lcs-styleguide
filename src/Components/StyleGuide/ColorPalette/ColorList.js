import React from "react";

import Color from "./Color";

const ColorList = ({ title, description, colors, extraClassOnList }) => {
  return (
    <div className="sg-color-palette">
      <div className="m-xsmall-bottom-1">
        <h2>{title}</h2>
        <p>{description}</p>
      </div>
      <ul className={`sg-color-list ${extraClassOnList}`}>
        {colors.map((color, index) => (
          <Color name={color} key={index} />
        ))}
      </ul>
    </div>
  );
};

export default ColorList;
