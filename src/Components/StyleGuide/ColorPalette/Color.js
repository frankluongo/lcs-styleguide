import React from "react";

const Color = ({ name }) => {
  const hexValue = getComputedStyle(document.documentElement).getPropertyValue(`--${name}`)
  const rgbValue = hexToRgb(hexValue);

  return (
    <li className={`sg-color text-align-center`}>
      <div className={`sg-color__color  sg-color--${name}`} />
      <div className="sg-color__title">
        <strong>{converColorName(name)}</strong>
        <p>{rgbValue}</p>
        <p>{hexValue}</p>
      </div>
    </li>
  );

  function converColorName(name) {
    return name.replace(/-/g, " ").toUpperCase();
  }

  function hexToRgb(hex) {
    const hexValuesOnly = hex.replace('#', '');
    const bigint = parseInt(hexValuesOnly, 16);

    return `rgb(${(bigint >> 16) & 255}, ${(bigint >> 8) & 255}, ${bigint & 255})`;
  }
};

Color.defaultProps = {
  name: ""
};

export default Color;
