import React from "react";

const TypeExample = ({ className, extraTypes, tag, text, title }) => {
  return (
    <div className="sg-type-example m-xsmall-bottom-1">
      <div className="sg-type-example__heading m-xsmall-bottom-1">
        <strong>{title}</strong>
      </div>
      <div className="sg-type-example__content">
        <div className="sg-type-example-content__base">
          {extraTypes.length > 0 && <strong>BASE</strong>}
          {React.createElement(
            tag,
            {
              className,
              title
            },
            text
          )}
        </div>
        {extraTypes.map((type, index) => (
          <div
            className="sg-type-example-content__variation m-xsmall-top-3"
            key={index}
          >
            <strong>{type.toUpperCase()}</strong>
            {React.createElement(
              tag,
              {
                className: `${className} ${type}`,
                title
              },
              text
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

TypeExample.defaultProps = {
  className: "",
  extraTypes: [],
  tag: "p",
  text:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
  title: "Sample"
};

export default TypeExample;
