import React from "react";

const Block = ({ children, modifiers, style }) => {
  return <div className={`block ${style} ${modifiers}`}>{children}</div>;
};

Block.defaultProps = {
  modifiers: "",
  style: "border" // Can also be "shadow" and "border with-cta"
};

export default Block;
