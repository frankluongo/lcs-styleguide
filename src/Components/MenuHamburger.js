import React, { useState, Fragment } from "react";
import { useSpring, animated } from "react-spring";

const MenuHamburger = () => {
  const [open, setMenuState] = useState(false);

  const animation = useSpring({
    transform: `translate3d(${open ? "0" : "-100"}%, 0, 0)`,
    pointerEvents: open ? "all" : "none"
  });

  const toggleHamburgermenu = () => {
    setMenuState(!open);
  };

  return (
    <Fragment>
      <button
        aria-label="Toggle Menu"
        className="hamburger-menu__toggle toggle"
        onClick={toggleHamburgermenu.bind(this)}
      >
        <span className="hamburger-menu-toggle__line toggle__item row-1" />
        <span className="hamburger-menu-toggle__line toggle__item row-2" />
        <span className="hamburger-menu-toggle__line toggle__item row-3" />
      </button>
      <animated.div className="hamburger-menu" style={animation}>
        <div className="hamburger-menu__header">
          <button
            aria-label="Close Menu"
            className="hamburger-menu__close"
            onClick={toggleHamburgermenu.bind(this)}
          >
            <span className="fa fa-times" />
          </button>
          <div className="hamburger-menu-list__heading h3">
            Hamburger Menu Header
          </div>
        </div>
        <div className="hamburger-menu__list-wrapper">
          <ul className="menu hamburger-menu__list">
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 1</li>
            <li className="hamburger-menu-list__item">Item 2</li>
          </ul>
        </div>
      </animated.div>
    </Fragment>
  );
};

export default MenuHamburger;
