import React from "react";

const BreadCrumbs = ({ crumbs }) => {
  const Crumb = ({ title, url }) => {
    return (
      <li className="breacrumbs__crumb">
        <a className="breadcrumbs-crumb__link ui-mini-copy" href={url}>
          {title}
        </a>
      </li>
    );
  };

  return (
    <ul className="breadcrumbs unstyled inline">
      {crumbs.map((crumb, index) => (
        <Crumb title={crumb.title} url={crumb.url} key={index} />
      ))}
    </ul>
  );
};

export default BreadCrumbs;
