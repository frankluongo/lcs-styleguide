import React from "react";

const TileContainer = ({ children }) => {
  return <div className="tile-container content-container">{children}</div>;
};

export default TileContainer;
