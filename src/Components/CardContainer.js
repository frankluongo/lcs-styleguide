import React from "react";

const CardContainer = ({ children }) => {
  return <section className="cards-container">{children}</section>;
};

export default CardContainer;
