import React, { Fragment } from "react";

import Header from "./Header";

const Layout = ({ children, mainBg }) => {
  return (
    <Fragment>
      <Header />
      <main className={`sg-main ${mainBg}`}>{children}</main>
    </Fragment>
  );
};

Layout.defaultProps = {
  mainBg: ""
};

export default Layout;
