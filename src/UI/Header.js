import React from "react";
import Navigation from "./Navigation";

const Header = () => {
  return (
    <header className="sg-header" aria-label="Website Header">
      <Navigation />
    </header>
  );
};

export default Header;
