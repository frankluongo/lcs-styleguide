import React from "react";

const Container = ({ children, type }) => {
  return <section className={`container ${type}`}>{children}</section>;
};

export default Container;
