import React from "react";

const Example = ({ children, type }) => {
  return <div className={`example ${type}`}>{children}</div>;
};

export default Example;
