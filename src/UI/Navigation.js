import React from "react";
import { Link } from "react-router-dom";

const Navigation = () => {
  return (
    <nav aria-label="Website">
      <ul className="navigation-list">
        <li>
          <Link className="link" to="/">
            Home
          </Link>
        </li>
        <li>
          <Link className="link" to="/color-palettes">
            Color Palettes
          </Link>
        </li>
        <li>
          <Link className="link" to="/typography">
            Typography
          </Link>
        </li>
        <li>
          <Link className="link" to="/elements">
            Elements
          </Link>
        </li>
        <li>
          <Link className="link" to="/components">
            Components
          </Link>
        </li>
        <li>
          <Link className="link" to="/layouts">
            Layouts
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default Navigation;
