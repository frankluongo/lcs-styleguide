import React from "react";

const Heading = ({ children }) => {
  return <div className="m-xsmall-bottom-1 m-xsmall-bottom-2">{children}</div>;
};

export default Heading;
