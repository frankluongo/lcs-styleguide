import React from "react";
import Layout from "../UI/Layout";
import Container from "../UI/Container";
import Column1 from "../Layout/Column1";
import Column2 from "../Layout/Column2";
import Column3 from "../Layout/Column3";
import Column4 from "../Layout/Column4";
import Column5 from "../Layout/Column5";
import Column6 from "../Layout/Column6";
import MainSidebarRight from "../Layout/MainSidebarRight";
import MainSidebarLeft from "../Layout/MainSidebarLeft";
import FlexLayout from "../Layout/Flex";
import Heading from "../UI/Heading";
import Block from "../Components/Block";

const Layouts = () => {
  return (
    <Layout>
      <Container>
        <Heading>
          <h1 className="m-xsmall-bottom-1">Layouts</h1>
          <p>
            This page contains common layout structures that will appear on our
            pages
          </p>
        </Heading>
      </Container>
      <Container type="sg-container">
        <Heading>
          <h2>Single Column Layout</h2>
        </Heading>
        <Column1>
          <Block>
            <div>
              Any content in here will take up the full amount of space alotted
            </div>
          </Block>
          <Block>
            <div>And all blocks will stack on top of one another</div>
          </Block>
        </Column1>
      </Container>
      <Container type="sg-container">
        <Heading>
          <h2>Split Column Layout</h2>
        </Heading>
        <Column2>
          <Block>1</Block>
          <Block>2</Block>
        </Column2>
      </Container>
      <Container type="sg-container">
        <Heading>
          <h2>Three Column Layout</h2>
        </Heading>
        <Column3>
          <Block>1</Block>
          <Block>2</Block>
          <Block>3</Block>
        </Column3>
      </Container>
      <Container type="sg-container">
        <Heading>
          <h2>Four Column Layout</h2>
        </Heading>
        <Column4>
          <Block>1</Block>
          <Block>2</Block>
          <Block>3</Block>
          <Block>4</Block>
        </Column4>
      </Container>
      <Container type="sg-container">
        <Heading>
          <h2>Five Column Layout</h2>
        </Heading>
        <Column5>
          <Block>1</Block>
          <Block>2</Block>
          <Block>3</Block>
          <Block>4</Block>
          <Block>5</Block>
        </Column5>
      </Container>
      <Container type="sg-container">
        <Heading>
          <h2>Six Column Layout</h2>
        </Heading>
        <Column6>
          <Block>1</Block>
          <Block>2</Block>
          <Block>3</Block>
          <Block>4</Block>
          <Block>5</Block>
          <Block>6</Block>
        </Column6>
      </Container>
      <Container type="sg-container">
        <Heading>
          <h2>Main With Right Sidebar</h2>
        </Heading>
        <MainSidebarRight>
          <Block>1</Block>
          <Block>2</Block>
        </MainSidebarRight>
      </Container>
      <Container type="sg-container">
        <Heading>
          <h2>Main With Left Sidebar</h2>
        </Heading>
        <MainSidebarLeft>
          <Block>1</Block>
          <Block>2</Block>
        </MainSidebarLeft>
      </Container>
      <Container type="sg-container">
        <Heading>
          <h2>Flex Layout</h2>
        </Heading>
        <FlexLayout>
          <Block>content</Block>
          <Block>will</Block>
          <Block>fill</Block>
          <Block>the</Block>
          <Block>space</Block>
          <Block>alotted</Block>
        </FlexLayout>
      </Container>
    </Layout>
  );
};

export default Layouts;
