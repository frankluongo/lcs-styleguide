import React from "react";

import Layout from "../UI/Layout";
import Heading from "../UI/Heading";

const Grid = () => {
  return (
    <Layout>
      <section className="container">
        <Heading>
          <h1>Grid</h1>
        </Heading>
      </section>

      <section className="container">
        <div className="grid-row">
          <div className="col col-xsmall-12">
            <div className="sg-col-example">Full width column</div>
          </div>
        </div>

        <div className="grid-row">
          <div className="col col-xsmall-12 col-small-6">
            <div className="sg-col-example">Full / Half width column</div>
          </div>
          <div className="col col-xsmall-12 col-small-6">
            <div className="sg-col-example">Full / Half width column</div>
          </div>
        </div>

        <div className="grid-row">
          <div className="col col-xsmall-4">
            <div className="sg-col-example">Third width column</div>
          </div>
          <div className="col col-xsmall-4">
            <div className="sg-col-example">Third width column</div>
          </div>
          <div className="col col-xsmall-4">
            <div className="sg-col-example">Third width column</div>
          </div>
        </div>

        <div className="grid-row">
          <div className="col col-xsmall-3">
            <div className="sg-col-example">Quarter width column</div>
          </div>
          <div className="col col-xsmall-3">
            <div className="sg-col-example">Quarter width column</div>
          </div>
          <div className="col col-xsmall-3">
            <div className="sg-col-example">Quarter width column</div>
          </div>
          <div className="col col-xsmall-3">
            <div className="sg-col-example">Quarter width column</div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default Grid;
