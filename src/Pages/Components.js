import React from "react";
import Layout from "../UI/Layout";
import Accordion from "../Components/Accordion";
import Container from "../UI/Container";
import Example from "../UI/Example";
import Block from "../Components/Block";
import BreadCrumbs from "../Components/BreadCrumbs";
import Card from "../Components/Card";
import CardContainer from "../Components/CardContainer";
import Carousel from "../Components/Carousel";
import MenuBento from "../Components/MenuBento";
import MenuHamburger from "../Components/MenuHamburger";
import MenuKebab from "../Components/MenuKebab";
import MenuMeatballs from "../Components/MenuMeatballs";
import MenuSidebar from "../Components/MenuSidebar";
import MessageBox from "../Components/MessageBox";
import Modal from "../Components/Modal";
import NotificationBar from "../Components/NotificationBar";
import SearchForm from "../Components/SearchForm";
import Tile from "../Components/Tile";
import Tooltip from "../Components/Tooltip";
import TileContainer from "../Components/TileContainer";
import MenuDropdown from "../Components/MenuDropdown";
import Tabs from "../Components/Tabs";

const Components = () => {
  return (
    <Layout>
      <section className="container">
        <h1>Common Web Components</h1>
      </section>

      <section className="container sg-component-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Accordion</h2>
        <div className="sg-component-example">
          <Accordion
            panels={[
              {
                title: "Panel Title",
                content: (
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Doloremque debitis nemo aperiam impedit error pariatur ex,
                    explicabo nulla exercitationem consequatur nesciunt, aliquam
                    dolor veniam perferendis sequi, accusantium rerum! Cumque,
                    omnis!
                  </p>
                )
              },
              {
                title: "Panel Title",
                content: (
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Doloremque debitis nemo aperiam impedit error pariatur ex,
                    explicabo nulla exercitationem consequatur nesciunt, aliquam
                    dolor veniam perferendis sequi, accusantium rerum! Cumque,
                    omnis!
                  </p>
                )
              }
            ]}
          />
        </div>
      </section>

      <Container type="sg-component-container">
        <div className="m-xsmall-bottom-1 m-small-bottom-2">
          <h2 className="m-xsmall-bottom-1">Block</h2>
          <p>A block is any self-contained piece of content</p>
        </div>
        <Example type="sg-component-example">
          <Block style="border">
            <p>A block can have any kind of content in it</p>
          </Block>
          <Block style="shadow">
            <p>A block can have any kind of content in it</p>
          </Block>
        </Example>
        <Example type="sg-component-example">
          <Block style="border with-cta">
            <p>It can also have a call to action</p>
            <a className="block__cta button primary small" href="#">
              Call To Action
            </a>
          </Block>
        </Example>
      </Container>

      <Container type="sg-component-container">
        <div className="m-xsmall-bottom-1 m-small-bottom-2">
          <h2 className="m-xsmall-bottom-1">Breadcrumbs</h2>
          <p>Breadcrumbs display a list of links to where a user has been</p>
        </div>
        <Example type="sg-component-example full">
          <BreadCrumbs
            crumbs={[
              {
                title: "Page Name",
                url: "#"
              },
              {
                title: "Page Name",
                url: "#"
              },
              {
                title: "Page Name",
                url: "#"
              },
              {
                title: "Page Name",
                url: "#"
              }
            ]}
          />
        </Example>
      </Container>

      <Container type="sg-component-container">
        <div className="m-xsmall-bottom-1 m-small-bottom-2">
          <h2 className="m-xsmall-bottom-1">Cards</h2>
          <p>Cards are a visually appealing way to group content</p>
        </div>
        <Example type="sg-component-example full">
          <CardContainer>
            <Card
              image="http://placekitten.com/g/200/300"
              title="Cute Kitten"
              description="lorem ipsum dolor sit amet and that's all I know so here's some fake filler copy yay lalala blah"
              url="#"
              cta="Click Me!"
            />
            <Card
              image="http://placekitten.com/g/200/300"
              title="Cute Kitten"
              description="lorem ipsum dolor sit amet and that's all I know so here's some fake filler copy yay lalala blah"
              url="#"
              cta="Click Me!"
            />
            <Card
              image="http://placekitten.com/g/200/300"
              title="Cute Kitten"
              description="lorem ipsum dolor sit amet and that's all I know so here's some fake filler copy yay lalala blah"
              url="#"
              cta="Click Me!"
            />
            <Card
              image="http://placekitten.com/g/200/300"
              title="Cute Kitten"
              description="lorem ipsum dolor sit amet and that's all I know so here's some fake filler copy yay lalala blah"
              url="#"
              cta="Click Me!"
            />
            <Card
              image="http://placekitten.com/g/200/300"
              title="Cute Kitten"
              description="lorem ipsum dolor sit amet and that's all I know so here's some fake filler copy yay lalala blah"
              url="#"
              cta="Click Me!"
            />
          </CardContainer>
        </Example>
      </Container>
      <Container type="sg-component-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Carousel</h2>
        <Example type="sg-component-example full">
          <Carousel />
        </Example>
      </Container>

      <Container type="sg-component-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Bento Menu</h2>
        <Example type="sg-component-example full">
          <MenuBento />
        </Example>
      </Container>
      <Container type="sg-component-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Burger Menu</h2>
        <Example type="sg-component-example full">
          <MenuHamburger />
        </Example>
      </Container>
      <Container type="sg-component-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Dropdown Menu</h2>
        <Example type="sg-component-example">
          <MenuDropdown />
          <MenuDropdown disabled={true} />
        </Example>
      </Container>
      <Container type="sg-component-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Kebab Menu</h2>
        <Example type="sg-component-example full">
          <MenuKebab />
        </Example>
      </Container>
      <Container type="sg-component-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Meatball Menu</h2>
        <Example type="sg-component-example full">
          <MenuMeatballs />
        </Example>
      </Container>
      <Container type="sg-component-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Sidebar Menu</h2>
        <Example type="layout main-sidebar-left ">
          <section>Main Content</section>
          <MenuSidebar />
        </Example>
      </Container>

      <Container type="sg-component-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Message Boxes</h2>
        <Example type="sg-component-example">
          <MessageBox
            type="info"
            title="Info Message Box"
            content={
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam
                voluptatum perspiciatis modi possimus nihil aliquam rem, unde
                veniam soluta assumenda. Temporibus molestiae qui quia fuga
                quibusdam asperiores a quis magnam.
              </p>
            }
          />
          <MessageBox
            type="success"
            title="Success Message Box"
            content={
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam
                voluptatum perspiciatis modi possimus nihil aliquam rem, unde
                veniam soluta assumenda. Temporibus molestiae qui quia fuga
                quibusdam asperiores a quis magnam.
              </p>
            }
          />
        </Example>
        <Example type="sg-component-example">
          <MessageBox
            type="error"
            title="Error Message Box"
            content={
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam
                voluptatum perspiciatis modi possimus nihil aliquam rem, unde
                veniam soluta assumenda. Temporibus molestiae qui quia fuga
                quibusdam asperiores a quis magnam.
              </p>
            }
          />
          <MessageBox
            type="warning"
            title="Warning Message Box"
            content={
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam
                voluptatum perspiciatis modi possimus nihil aliquam rem, unde
                veniam soluta assumenda. Temporibus molestiae qui quia fuga
                quibusdam asperiores a quis magnam.
              </p>
            }
          />
        </Example>
      </Container>

      <Container type="sg-component-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Modal</h2>
        <Example type="sg-component-example full">
          <Modal>Modal Content</Modal>
        </Example>
      </Container>

      <Container type="sg-component-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Notification Bar</h2>
        <Example type="sg-component-example full">
          <NotificationBar />
        </Example>
      </Container>

      <Container type="sg-component-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Search Form</h2>
        <Example type="sg-component-example full">
          <SearchForm />
        </Example>
      </Container>

      <Container type="sg-component-container">
        <h2 className="m-xsmall-bottom-1">Tabs</h2>
        <Example type="sg-component-example full">
          <Tabs />
        </Example>
      </Container>

      <Container type="sg-component-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Tile</h2>
        <Example type="sg-component-example full">
          <TileContainer>
            <Tile />
            <Tile />
            <Tile />
            <Tile />
            <Tile />
          </TileContainer>
        </Example>
      </Container>

      <Container type="sg-component-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Tooltip</h2>
        <Example type="sg-component-example full">
          <p>
            When there is text that needs clarification, we have a tooltip.{" "}
            <Tooltip />
          </p>
        </Example>
      </Container>
    </Layout>
  );
};

export default Components;
