import React from "react";

import Layout from "../UI/Layout";
import ColorList from "../Components/StyleGuide/ColorPalette/ColorList";

const ColorPalette = () => {
  return (
    <Layout>
      <section className="container">
        <h1 className="m-xsmall-bottom-1 m-small-bottom-2">Color Palette</h1>
      </section>
      <div className="container" aria-label="Color Palettes">
        <section
          aria-label="CGA Color Palettes"
          className="sg-color-palette-container"
        >
          <ColorList
            title="CGA Official Primary Color Palette"
            description="These colors are the first option for theming an element or component"
            colors={["base-lcs-blue", "base-lcs-gold", "base-lcs-grey"]}
          />
          <ColorList
            title="CGA Official Blue Expanded Colorways"
            colors={[
              "lcs-blue-1",
              "lcs-blue-2",
              "lcs-blue-3",
              "lcs-blue-4",
              "lcs-blue-5",
              "lcs-blue-6"
            ]}
          />
          <ColorList
            title="CGA Official Gold Expanded Colorways"
            colors={[
              "lcs-gold-1",
              "lcs-gold-2",
              "lcs-gold-3",
              "lcs-gold-4",
              "lcs-gold-5",
              "lcs-gold-6"
            ]}
          />
          <ColorList
            title="CGA Official Web Friendly Gold Expanded Colorways"
            colors={[
              "lcs-gold-web-1",
              "lcs-gold-web-2",
              "lcs-gold-web-3",
              "lcs-gold-web-4"
            ]}
          />
          <ColorList
            title="CGA Official Grey Expanded Colorways"
            colors={[
              "lcs-grey-1",
              "lcs-grey-2",
              "lcs-grey-3",
              "lcs-grey-4",
              "lcs-grey-5",
              "lcs-grey-6"
            ]}
          />
        </section>

        <section
          className="container sg-color-palette-container"
          aria-label="Colorado General Assembly Secondary Color Palettes"
        >
          <ColorList
            title="CGA Official Secondary Color Palette"
            description="These colors are the second option for theming an element or
            component"
            colors={[
              "base-lcs-secondary-purple",
              "base-lcs-secondary-mauve",
              "base-lcs-secondary-red",
              "base-lcs-secondary-teal",
              "base-lcs-secondary-green",
              "base-lcs-secondary-orange"
            ]}
          />
          <ColorList
            title="CGA Official Purple Expanded Colorways"
            colors={[
              "lcs-purple-1",
              "lcs-purple-2",
              "lcs-purple-3",
              "lcs-purple-4",
              "lcs-purple-5",
              "lcs-purple-6"
            ]}
          />
          <ColorList
            title="CGA Official Mauve Expanded Colorways"
            colors={[
              "lcs-mauve-1",
              "lcs-mauve-2",
              "lcs-mauve-3",
              "lcs-mauve-4",
              "lcs-mauve-5",
              "lcs-mauve-6"
            ]}
          />
          <ColorList
            title="CGA Official Red Expanded Colorways"
            colors={[
              "lcs-red-1",
              "lcs-red-2",
              "lcs-red-3",
              "lcs-red-4",
              "lcs-red-5",
              "lcs-red-6"
            ]}
          />
          <ColorList
            title="CGA Official Teal Expanded Colorways"
            colors={[
              "lcs-teal-1",
              "lcs-teal-2",
              "lcs-teal-3",
              "lcs-teal-4",
              "lcs-teal-5",
              "lcs-teal-6"
            ]}
          />
          <ColorList
            title="CGA Official Green Expanded Colorways"
            colors={[
              "lcs-green-1",
              "lcs-green-2",
              "lcs-green-3",
              "lcs-green-4",
              "lcs-green-5",
              "lcs-green-6"
            ]}
          />
          <ColorList
            title="CGA Official Orange Expanded Colorways"
            colors={[
              "lcs-orange-1",
              "lcs-orange-2",
              "lcs-orange-3",
              "lcs-orange-4",
              "lcs-orange-5",
              "lcs-orange-6"
            ]}
          />
        </section>

        <section
          className="container sg-color-palette-container"
          aria-label="United States Web Design Color Palettes"
        >
          <ColorList
            title="United States Web Design Grayscale Color Palette + 1"
            description="Used for coloring Text and/or background elements in order to
            provide a greater range of contrast"
            colors={[
              "gray-1",
              "gray-2",
              "gray-3",
              "gray-4",
              "gray-5",
              "gray-6",
              "gray-7",
              "gray-8"
            ]}
            extraClassOnList="eight-up"
          />
          <ColorList
            title="United States Web Design Info Color Palette"
            description="Used when providing information, clarification or context to a
            user"
            colors={["info-1", "info-2", "info-3", "info-4", "info-5"]}
          />
          <ColorList
            title="United States Web Design Error Color Palette"
            description="Used to convey when something has gone wrong to a user"
            colors={["error-1", "error-2", "error-3", "error-4", "error-5"]}
          />
          <ColorList
            title="United States Web Design Warning Color Palette"
            description="Used to convey a warning message to a user of potential error or
            problems"
            colors={[
              "warning-1",
              "warning-2",
              "warning-3",
              "warning-4",
              "warning-5"
            ]}
          />
          <ColorList
            title="United States Web Design Success Color Palette"
            description="Used to convey a successful completion of an action"
            colors={[
              "success-1",
              "success-2",
              "success-3",
              "success-4",
              "success-5"
            ]}
          />
          <ColorList
            title="United States Web Design Disabled Color Palette"
            description="Used to indicate that a feature or element is disabled/unusable to
            a user"
            colors={["disabled-1", "disabled-2", "disabled-3"]}
          />
        </section>

        <section
          className="container sg-color-palette-container"
          aria-label="Colorado Color Palettes"
        >
          <ColorList
            title="State of Colorado Official Color Palette"
            description="These colors serve as an alternative option for coloring/theming
            an application"
            colors={[
              "base-co-green",
              "base-co-orange",
              "base-co-blue",
              "base-co-brown",
              "base-co-silver",
              "base-co-slate",
              "base-co-black"
            ]}
          />
          <ColorList
            title="State of Colorado Official Green Expanded Colorways"
            colors={[
              "green-1",
              "green-2",
              "green-3",
              "green-4",
              "green-5",
              "green-6",
              "green-7"
            ]}
          />
          <ColorList
            title="State of Colorado Official Orange Expanded Colorways"
            colors={[
              "orange-1",
              "orange-2",
              "orange-3",
              "orange-4",
              "orange-5",
              "orange-6",
              "orange-7"
            ]}
          />
          <ColorList
            title="State of Colorado Official Blue Expanded Colorways"
            colors={[
              "blue-1",
              "blue-2",
              "blue-3",
              "blue-4",
              "blue-5",
              "blue-6",
              "blue-7"
            ]}
          />
          <ColorList
            title="State of Colorado Official Brown Expanded Colorways"
            colors={[
              "brown-1",
              "brown-2",
              "brown-3",
              "brown-4",
              "brown-5",
              "brown-6",
              "brown-7"
            ]}
          />
        </section>
      </div>
    </Layout>
  );
};

export default ColorPalette;
