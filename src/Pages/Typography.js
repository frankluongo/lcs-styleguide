import React from "react";
import Layout from "../UI/Layout";

import TypeExample from "../Components/StyleGuide/TypeExample";

const Typography = () => {
  return (
    <Layout>
      <section className="container">
        <h1 className="m-xsmall-bottom-1 m-small-bottom-2">Typography</h1>
      </section>

      <section className="container sg-typography-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">
          Base Typography Elements
        </h2>
        <hr className="m-xsmall-bottom-1 m-small-bottom-2" />

        <TypeExample
          tag="h1"
          text="Page Headings"
          title="H1"
          extraTypes={["xlarge", "large", "small"]}
        />
        <TypeExample
          tag="h2"
          text="Section Headings"
          title="H2"
          extraTypes={["large", "small"]}
        />
        <TypeExample tag="h3" text="H3: Sub/Section Headings" />
        <TypeExample tag="h4" text="H4: Paragraph Headings" />
        <TypeExample
          tag="p"
          text="Paragraph Tag: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "
        />
        <TypeExample tag="strong" text="Strong Tag, Bold Text" />
        <TypeExample tag="b" text="B Tag, Bold Text" />
        <TypeExample tag="i" text="I Tag, Italic Text" />
        <TypeExample tag="em" text="Em Tag, Emphasized Text" />
        <TypeExample tag="mark" text="Mark Tag, Highlighted Text" />
        <TypeExample tag="small" text="Small Tag, Smaller Text" />
        <TypeExample tag="del" text="Del Tag, Deleted Text" />
        <TypeExample tag="ins" text="Ins Tag, Inserted Text" />
        <TypeExample tag="sub" text="SUB Tag, Subscript Text" />
        <TypeExample tag="sup" text="SUP Tag, Superscript Text" />
        <TypeExample tag="address" text="Address Tag, Used for an Address" />
        <TypeExample tag="cite" text="Cite Tag, Citation Text" />
        <TypeExample
          tag="abbr"
          title="Abbreviated Text"
          text="Abbreviation Tag, abbreviated Text"
        />
        <TypeExample tag="q" text="Q Tag, Short Quote Text" />
        <TypeExample tag="blockquote" text="Blockquote Tag, Long Quote Text" />
        <TypeExample tag="a" className="link" text="A Tag, Used for links" />
        <TypeExample
          tag="button"
          className="button primary default"
          text="Button Tag, Used for buttons"
        />
        <TypeExample
          tag="label"
          className="input-label"
          text="Label Tag with input-label class, Used for Inputs"
        />
        <TypeExample
          tag="label"
          className="checkbox-label"
          text="Label Tag with checkbox-label class, Used for checkboxes and radio buttons"
        />
        <TypeExample
          tag="legend"
          text="Legend Tag, used for a heading when multiple options are grouped"
        />
      </section>

      <section className="container sg-typography-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">
          UI Typography Elements
        </h2>
        <hr className="m-xsmall-bottom-1 m-small-bottom-2" />

        <TypeExample
          tag="div"
          className="ui-mini-copy"
          text="Mini Copy - Smaller Copy"
        />

        <TypeExample
          tag="div"
          className="ui-micro-copy"
          text="Micro Copy - Smallest Copy"
        />
      </section>
    </Layout>
  );
};

export default Typography;
