import React from "react";
import Layout from "../UI/Layout";

const Elements = () => {
  return (
    <Layout>
      <section className="container sg-element-container">
        <h1 className="m-xsmall-bottom-1">Elements</h1>
        <p>
          <strong>Elements</strong> refer to HTML elements (and groups of
          elements)
        </p>
      </section>

      <section className="container sg-element-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Links (A Tags)</h2>

        <div className="sg-element-example">
          <div className="bg-gray-1 p-xsmall-1">
            <a className="link" href="#">
              Default Link for light backgrounds
            </a>
          </div>
          <div className="bg-gray-8 p-xsmall-1">
            <a className="link alt" href="#">
              Alternative Link for dark backgrounds
            </a>
          </div>
        </div>
      </section>

      <section className="container sg-element-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Buttons</h2>

        <div className="sg-element-example flex">
          <button className="button primary small m-xsmall-right-1">
            Small Button
          </button>
          <button className="button primary default m-xsmall-right-1">
            Default Button
          </button>
          <button className="button primary large">Large Button</button>
        </div>

        <div className="sg-element-example">
          <div className="bg-gray-1 p-xsmall-1">
            <button className="button primary default">Primary Button</button>
          </div>
          <div className="bg-gray-8 p-xsmall-1">
            <button className="button primary alt default">
              Alt Primary Button
            </button>
          </div>
        </div>
        <div className="sg-element-example">
          <div className="bg-gray-1 p-xsmall-1">
            <button className="button secondary default">
              Secondary Button
            </button>
          </div>
          <div className="bg-gray-8 p-xsmall-1">
            <button className="button secondary alt default">
              Alt Secondary Button
            </button>
          </div>
        </div>
        <div className="sg-element-example">
          <div className="bg-gray-1 p-xsmall-1">
            <button className="button tertiary default">tertiary Button</button>
          </div>
          <div className="bg-gray-8 p-xsmall-1">
            <button className="button tertiary alt default">
              Alt tertiary Button
            </button>
          </div>
        </div>
        <div className="sg-element-example">
          <div className="bg-gray-1 p-xsmall-1">
            <button className="button danger default">danger Button</button>
          </div>
          <div className="bg-gray-8 p-xsmall-1">
            <button className="button danger alt default">
              Alt danger Button
            </button>
          </div>
        </div>
        <div className="sg-element-example">
          <div className="bg-gray-1 p-xsmall-1">
            <button className="button success default">success Button</button>
          </div>
          <div className="bg-gray-8 p-xsmall-1">
            <button className="button success alt default">
              Alt success Button
            </button>
          </div>
        </div>

        <div className="sg-element-example">
          <div className="bg-gray-1 p-xsmall-1">
            <button className="button primary default with-icon">
              Button w/ Icon
              <span className="fa fa-chevron-circle-right"></span>
            </button>
          </div>
          <div className="bg-gray-8 p-xsmall-1">
            <button className="button primary alt default with-icon">
              Button w/ Icon
              <span className="fa fa-chevron-circle-right"></span>
            </button>
          </div>
        </div>
      </section>

      <section className="container sg-element-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Figures</h2>

        <div className="sg-element-example">
          <div>
            <figure>
              <img
                src="https://placekitten.com/640/360"
                alt="Look at these kittens"
              />
              <figcaption>This is a caption for an image</figcaption>
            </figure>
          </div>
          <div>
            <figure>
              <div className="figcontent">
                <pre>
                  {`const car = new SelfDrivingCar(); if (goingToHitSomeone){" "}
                  car.dont
                  `}
                </pre>
              </div>
              <figcaption>This is a caption for code</figcaption>
            </figure>
          </div>
        </div>
        <div className="sg-element-example">
          <div>
            <p>
              <strong>Figure with no border</strong>
            </p>
            <figure className="borderless">
              <img
                src="https://placekitten.com/640/360"
                alt="Look at these kittens"
              />
              <figcaption>This is a caption for an image</figcaption>
            </figure>
          </div>
          <div></div>
        </div>
      </section>

      <section className="container sg-form-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Forms</h2>

        <div className="sg-form-example">
          <div className="form-control">
            <label className="input-label" htmlFor="input-sample-default">
              Text Field
            </label>
            <input
              type="text"
              name="Sample"
              id="input-sample-default"
              placeholder="Placeholder"
            />
          </div>
          <div className="form-control">
            <label className="input-label" htmlFor="input-sample-disabled">
              Disabled Text Field
            </label>
            <input
              type="text"
              name="Sample"
              id="input-sample-disabled"
              placeholder="Placeholder"
              disabled
              aria-disabled="true"
            />
          </div>
        </div>

        <div className="sg-form-example">
          <div className="form-control">
            <label className="input-label" htmlFor="input-sample-valid">
              Valid Text Field
            </label>
            <input
              type="text"
              name="Sample"
              id="input-sample-valid"
              placeholder="Placeholder"
              data-valid
            />
          </div>
          <div className="form-control">
            <label
              className="input-label"
              htmlFor="input-sample-invalid"
              aria-invalid="true"
            >
              Invalid Text Field
            </label>
            <input
              type="text"
              name="Sample"
              id="input-sample-invalid"
              placeholder="Placeholder"
              aria-invalid="true"
            />
          </div>
        </div>

        <div className="sg-form-example">
          <div className="form-control">
            <label className="input-label" htmlFor="select-sample-default">
              Default Select Field
            </label>
            <div className="form-element-wrapper">
              <select name="Select Sample" id="select-sample-default">
                <option>Option 1</option>
                <option>Option 2</option>
                <option>Option 3</option>
                <option>Option 4</option>
              </select>
              <i className="form-icon fa fa-chevron-down"></i>
            </div>
          </div>
          <div className="form-control">
            <label className="input-label" htmlFor="select-sample-default">
              Disabled Select Field
            </label>
            <div className="form-element-wrapper">
              <select name="Select Sample" id="select-sample-default" disabled>
                <option>Option 1</option>
                <option>Option 2</option>
                <option>Option 3</option>
                <option>Option 4</option>
              </select>
              <i className="form-icon fa fa-chevron-down"></i>
            </div>
          </div>
        </div>

        <div className="sg-form-example">
          <div className="form-control">
            <label className="input-label" htmlFor="select-sample-default">
              Invalid Select Field
            </label>
            <div className="form-element-wrapper">
              <select
                name="Select Sample"
                id="select-sample-default"
                aria-invalid="true"
              >
                <option>--</option>
                <option>Option 1</option>
                <option>Option 2</option>
                <option>Option 3</option>
                <option>Option 4</option>
              </select>
              <i className="form-icon fa fa-chevron-down"></i>
            </div>
          </div>
        </div>

        <div className="sg-form-example">
          <div className="form-control">
            <label className="input-label" htmlFor="textarea-sample-default">
              Default Textarea
            </label>
            <textarea
              name="Sample"
              id="textarea-sample-default"
              cols="30"
              rows="10"
            ></textarea>
          </div>
          <div className="form-control">
            <label className="input-label" htmlFor="textarea-sample-disabled">
              Disabled Textarea
            </label>
            <textarea
              name="Sample"
              id="textarea-sample-disabled"
              cols="30"
              rows="10"
              disabled
            ></textarea>
          </div>
        </div>

        <div className="sg-form-example">
          <div className="form-control">
            <label
              className="input-label"
              htmlFor="textarea-sample-invalid"
              aria-invalid="true"
            >
              Invalid Textarea
            </label>
            <textarea
              name="Sample"
              id="textarea-sample-invalid"
              cols="30"
              rows="10"
              aria-invalid="true"
            ></textarea>
          </div>
        </div>

        <div className="sg-form-example">
          <fieldset className="form-control">
            <legend>Sample Radio Field</legend>
            <div className="form-option-wrapper">
              <input type="radio" name="sample-radio" id="sample-radio-1" />
              <span className="radio"></span>
              <label className="radio-label" htmlFor="sample-radio-1">
                Sample Radio 1
              </label>
            </div>
            <div className="form-option-wrapper">
              <input type="radio" name="sample-radio" id="sample-radio-2" />
              <span className="radio"></span>
              <label className="radio-label" htmlFor="sample-radio-2">
                Sample Radio 2
              </label>
            </div>
            <div className="form-option-wrapper">
              <input type="radio" name="sample-radio" id="sample-radio-3" />
              <span className="radio"></span>
              <label className="radio-label" htmlFor="sample-radio-3">
                Sample Radio 3
              </label>
            </div>
          </fieldset>

          <fieldset className="form-control" disabled aria-disabled="true">
            <legend>Disabled Sample Radio Field</legend>
            <div className="form-option-wrapper">
              <input
                type="radio"
                name="sample-radio-disabled"
                id="sample-radio-disabled-1"
              />
              <span className="radio"></span>
              <label className="radio-label" htmlFor="sample-radio-disabled-1">
                Sample Radio 1
              </label>
            </div>
            <div className="form-option-wrapper">
              <input
                type="radio"
                name="sample-radio-disabled"
                id="sample-radio-disabled-2"
              />
              <span className="radio"></span>
              <label className="radio-label" htmlFor="sample-radio-disabled-2">
                Sample Radio 2
              </label>
            </div>
            <div className="form-option-wrapper">
              <input
                type="radio"
                name="sample-radio-disabled"
                id="sample-radio-disabled-3"
              />
              <span className="radio"></span>
              <label className="radio-label" htmlFor="sample-radio-disabled-3">
                Sample Radio 3
              </label>
            </div>
          </fieldset>
        </div>

        <div className="sg-form-example">
          <fieldset className="form-control" aria-invalid="true">
            <legend>Invalid Sample Radio Field</legend>
            <div className="form-option-wrapper">
              <input
                type="radio"
                name="sample-radio-invalid"
                id="sample-radio-invalid-1"
              />
              <span className="radio"></span>
              <label className="radio-label" htmlFor="sample-radio-invalid-1">
                Sample Radio 1
              </label>
            </div>
            <div className="form-option-wrapper">
              <input
                type="radio"
                name="sample-radio-invalid"
                id="sample-radio-invalid-2"
              />
              <span className="radio"></span>
              <label className="radio-label" htmlFor="sample-radio-invalid-2">
                Sample Radio 2
              </label>
            </div>
            <div className="form-option-wrapper">
              <input
                type="radio"
                name="sample-radio-invalid"
                id="sample-radio-invalid-3"
              />
              <span className="radio"></span>
              <label className="radio-label" htmlFor="sample-radio-invalid-3">
                Sample Radio 3
              </label>
            </div>
          </fieldset>
        </div>
        <div className="sg-form-example">
          <div className="form-option-wrapper form-control">
            <input
              type="checkbox"
              name="sample-radio-invalid"
              id="sample-checkbox-1"
            />
            <span className="checkbox"></span>
            <label className="checkbox-label" htmlFor="sample-checkbox-1">
              Sample Checkbox 1
            </label>
          </div>
        </div>
      </section>

      <section className="container sg-element-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Lists</h2>

        <div className="sg-element-example">
          <div>
            <p>
              <strong>Unordered List</strong>
            </p>
            <ul>
              <li>This is an unordered list</li>
              <li>It has items</li>
              <li>In no particular order</li>
            </ul>
          </div>
          <div>
            <p>
              <strong>Unstyled Unordered List</strong>
            </p>
            <ul className="unstyled">
              <li>This is an unordered list with no styles</li>
              <li>It has items</li>
              <li>In no particular order</li>
              <li>with no bullets or padding</li>
            </ul>
          </div>
        </div>
        <div className="sg-element-example full">
          <div className="p-xsmall-top-1">
            <p>
              <strong>Inline Unordred List</strong>
            </p>
            <ul className="inline">
              <li>This is an inline, unordered list</li>
              <li>It has items</li>
              <li>arranged horizontally</li>
            </ul>
          </div>
        </div>
        <div className="sg-element-example full">
          <div className="p-xsmall-top-1">
            <p>
              <strong>Ordered List</strong>
            </p>
            <ol>
              <li>This is an ordered list</li>
              <li>It has items</li>
              <li>in a particular order</li>
            </ol>
          </div>
        </div>
        <div className="sg-element-example full">
          <div className="p-xsmall-top-1">
            <p>
              <strong>Description List</strong>
            </p>
            <dl>
              <dt>The description Term</dt>
              <dd>It's followed by a description definition</dd>

              <dt>The description Term</dt>
              <dd>It's followed by a description definition</dd>

              <dt>The description Term</dt>
              <dd>It's followed by a description definition</dd>
            </dl>
          </div>
        </div>
      </section>

      <section className="container sg-element-container">
        <h2 className="m-xsmall-bottom-1 m-small-bottom-2">Tables</h2>

        <div className="sg-element-example full">
          <table>
            <caption>Thursday, October 17th, 2019</caption>
            <thead>
              <tr>
                <th>Date/Time</th>
                <th>Activity</th>
                <th>Location</th>
                <th>Calendar</th>
                <th>Audio</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>7:30AM</td>
                <td>
                  <a className="link" href="#">
                    Capital Development Committee
                  </a>
                </td>
                <td>Off-Site</td>
                <td>
                  <a className="link" href="#">
                    Agenda
                  </a>
                </td>
                <td>
                  <a
                    className="link icon"
                    href="#"
                    aria-label="Listen to audio from this meeting"
                  >
                    <span className="fa fa-volume-up"></span>
                  </a>
                </td>
              </tr>
              <tr>
                <td>7:30AM</td>
                <td>
                  <a className="link" href="#">
                    Capital Development Committee
                  </a>
                </td>
                <td>Off-Site</td>
                <td>
                  <a className="link" href="#">
                    Agenda
                  </a>
                </td>
                <td>
                  <a
                    className="link icon"
                    href="#"
                    aria-label="Listen to audio from this meeting"
                  >
                    <span className="fa fa-volume-up"></span>
                  </a>
                </td>
              </tr>
              <tr>
                <td>7:30AM</td>
                <td>
                  <a className="link" href="#">
                    Capital Development Committee
                  </a>
                </td>
                <td>Off-Site</td>
                <td>
                  <a className="link" href="#">
                    Agenda
                  </a>
                </td>
                <td>
                  <a
                    className="link icon"
                    href="#"
                    aria-label="Listen to audio from this meeting"
                  >
                    <span className="fa fa-volume-up"></span>
                  </a>
                </td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <th colSpan="2">Total</th>
                <td>Amount</td>
              </tr>
            </tfoot>
          </table>
        </div>
      </section>
    </Layout>
  );
};

export default Elements;
