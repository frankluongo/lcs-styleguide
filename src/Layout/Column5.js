import React from "react";

const Column5 = ({ children }) => {
  return <section className="layout columns columns-5">{children}</section>;
};

export default Column5;
