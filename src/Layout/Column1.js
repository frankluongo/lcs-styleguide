import React from "react";

const Column1 = ({ children }) => {
  return <section className="layout columns column-1">{children}</section>;
};

export default Column1;
