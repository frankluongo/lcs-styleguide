import React from "react";

const Column2 = ({ children }) => {
  return <section className="layout columns columns-2">{children}</section>;
};

export default Column2;
