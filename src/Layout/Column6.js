import React from "react";

const Column6 = ({ children }) => {
  return <section className="layout columns columns-6">{children}</section>;
};

export default Column6;
