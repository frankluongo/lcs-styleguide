import React from "react";

const FlexLayout = ({ children, wrapClass }) => {
  return <section className={`layout flex ${wrapClass}`}>{children}</section>;
};

FlexLayout.defaultProps = {
  wrapClass: ""
};

export default FlexLayout;
