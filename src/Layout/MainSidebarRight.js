import React from "react";

const MainSidebarRight = ({ children }) => {
  return <section className="layout main-sidebar-right">{children}</section>;
};

export default MainSidebarRight;
