import React from "react";

const MainSidebarLeft = ({ children }) => {
  return <section className="layout main-sidebar-left">{children}</section>;
};

export default MainSidebarLeft;
