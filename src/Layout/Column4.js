import React from "react";

const Column4 = ({ children }) => {
  return <section className="layout columns columns-4">{children}</section>;
};

export default Column4;
