import React from "react";

const Column3 = ({ children }) => {
  return <section className="layout columns columns-3">{children}</section>;
};

export default Column3;
