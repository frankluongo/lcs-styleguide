// import Dropdown from './assets/scripts/modules/dropdown.js';
// import Modal from './assets/scripts/modules/modal.js';

// document.addEventListener('DOMContentLoaded', () => {
//   new Dropdown();
//   new Modal();
// });

import React from "react";
import ReactDOM from "react-dom";
import { Route, BrowserRouter as Router } from "react-router-dom";

import Application from "./src/Application";
import ColorPalette from "./src/Pages/ColorPalette";
import Components from "./src/Pages/Components";
import Elements from "./src/Pages/Elements";
import Grid from "./src/Pages/Grid";
import Layouts from "./src/Pages/Layouts";
import Typography from "./src/Pages/Typography";

const Routes = (
  <Router>
    <Route exact path="/" component={Application} />
    <Route exact path="/color-palettes" component={ColorPalette} />
    <Route exact path="/components" component={Components} />
    <Route exact path="/elements" component={Elements} />
    <Route exact path="/grid" component={Grid} />
    <Route exact path="/layouts" component={Layouts} />
    <Route exact path="/typography" component={Typography} />
  </Router>
);

ReactDOM.render(Routes, document.getElementById("app"));
